﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class AreaMap: ClassMap<Area>
    {
        public AreaMap()
        {
            Id(a => a.Id, "AreaID");
            Map(a => a.Name);
            HasManyToMany(a => a.ZipCodes).Cascade.All()
                .ParentKeyColumn("AreaID")
                .ChildKeyColumn("Code")
                .Table("PartOf");

        }
    }
}
