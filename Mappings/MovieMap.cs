﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class MovieMap : ClassMap<Movie>
    {
        public MovieMap()
        {
            Table("Movies");
            Schema("dbo");
            Id(m => m.Id, "MovieID");
            Map(m => m.Title, "MovieTitle");
            Map(m => m.Director);
            Map(m => m.Year);
            Map(m => m.Language);
            Map(m => m.Rating);
            Map(m => m.RunningTimeInMinutes, "RunningTime");         
        }
    }
}
