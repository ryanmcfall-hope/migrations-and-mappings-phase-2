﻿using System.Data.SqlClient;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;

namespace Mappings
{
    public class SessionFactory
    {
        public static ISessionFactory CreateSessionFactory()
        {
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = "sql.cs.hope.edu\\CSSQL,1433",
                IntegratedSecurity = false,
                InitialCatalog = System.Environment.GetEnvironmentVariable("392_USERNAME"),
                UserID = System.Environment.GetEnvironmentVariable("392_USERNAME"),
                Password = System.Environment.GetEnvironmentVariable("392_PASSWORD")
            };

            return Fluently.Configure()
              .Database(
                MsSqlConfiguration.MsSql2012
                  .ConnectionString(builder.ConnectionString)
                  .ShowSql()
                  .DefaultSchema("VideoStore")
                )              
              .Mappings(m => m.FluentMappings.AddFromAssemblyOf<CustomerMap>())
              .ExposeConfiguration(x => x.CurrentSessionContext<ThreadLocalSessionContext>())
              .BuildSessionFactory();
        }
    }
}
