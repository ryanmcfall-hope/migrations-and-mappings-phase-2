use File::Find;
use Getopt::Long;

require Term::ReadKey;

$executable = "";

sub findExe {
  my $current = $File::Find::name;
  if ($current =~ /Migrate\.exe$/) {
    if ($current =~ m@/any/@i) {
      $executable = $current;
    }
  }
}

sub printUsage {
  print "Usage:\n";
  print "perl Migrate.pl --task up|down|rollback|list --database database_name [--version version_number] [--server servername] [--username username] [--password password]\n";
  print "--version is required if --task is rollback\n";
  print "--server defaults to (localdb)\\MSSQLLocalDB\n";
  print "--username and --password are required if server is not localdb\n";
  exit (-1);
}

$task = "";
$version = "";
$server = "(localdb)\\MSSQLLocalDB";
$database = "";
$username = "";
$password="";

GetOptions ("task=s" => \$task,
            "version=i" => \$version,
            "server=s" => \$server,
            "database=s" => \$database,
            "username=s" => \$username,
            "password=s" => \$password
)
or printUsage();

lc $task;

if ($task ne "up" && $task ne "down" && $task ne "rollback" && $task ne "list") {
  print "Invalid task $task\n";
  printUsage();
}

if ($database eq "") {
  print "You must enter a value for the --database argument\n";
  printUsage();
}

if ($task eq "rollback" && $version eq "") {
  print "You must enter a value for --version if task is 'rollback'\n";
  print "The value of --version should be the migration number you want to rollback to\n";
  exit (-1);
}

if (!($server =~ /localdb/i)) {
  $integratedSecurity="false";
  $entryRequired = 0;
  if ($username eq "") {
    $entryRequired = 1;
    print "User name: ";
    chomp ($username = <STDIN>);
  }

  if ($password eq "") {
    $entryRequired = 1;
    Term::ReadKey::ReadMode('noecho');

    print "Password: ";
    chomp ($password = Term::ReadKey::ReadLine(0));

    # Reset the terminal to what it was previously doing
    Term::ReadKey::ReadMode('restore');
  }

  print "\nYou can provide the username and/or password on the command line using the --username and --password arguments\n" if $entryRequired;
}
else {
  $integratedSecurity = "true";
}

$projectDir = ".";

if ($task eq "rollback") {
  $task = "--task rollback:toversion --version $version";
}
elsif ($task ne "list") {
  $task = "--task migrate:$task";
}
else {
  $task = "--task listmigrations";
}

$packageDir = "$projectDir/packages";
opendir (projectDir, $packageDir) or die "Cannot opendir $packageDir";
@FILES = readdir(projectDir);
@results = grep (/FluentMigrator\.Console/, @FILES);
die "Can't find FluentMigrator.Console in $packageDir" if (@results == 0) ;

$FluentMigrator = $results[0];

$startingDirectory = "$projectDir\\packages\\$FluentMigrator";

find(\&findExe, ( $startingDirectory ) );

if ($executable eq "") {
  print "Could not find Migrate.exe in $startingDirectory\n.  Please make sure you're in the solution directory, not the project directory\n";
}
else {
  print "Found Migrate.exe executable in $executable\n\n";
}

$cmd = "$executable" .
       " --conn \"server=$server;Trusted_Connection=yes;Integrated Security=$integratedSecurity;Initial Catalog=$database";

$cmd .= ";UID=$username" if ($username ne ""); 
$cmd .= ";Password=$password" if ($password ne "");

$cmd .= "\" -provider sqlserver2008 --assembly Migrations\\bin\\Debug\\Migrations.dll $task";

print "Executing $cmd\n";

system($cmd);
