﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(11)]
    public class AddNameColumnsToCustomer : Migration
    {
        public override void Up()
        {
            Alter.Table("Customer").InSchema(Names.Schema)
                .AddColumn("MiddleName").AsString(50).Nullable()
                .AddColumn("Suffix").AsString(20).Nullable();
        }

        public override void Down()
        {
            Delete.Column("MiddleName").FromTable("Customer").InSchema(Names.Schema);
            Delete.Column("Suffix").FromTable("Customer").InSchema(Names.Schema);
        }
    }
}
