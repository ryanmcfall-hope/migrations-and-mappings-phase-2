﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(3)]
    public class PartOf : Migration
    {
        public override void Up()
        {
            Create.Table("PartOf").InSchema(Names.Schema)
                .WithColumn("AreaID").AsInt64().PrimaryKey()
                .WithColumn("Code").AsString(10).PrimaryKey();

            Create.ForeignKey("PartOfToZipCode")
                .FromTable("PartOf").InSchema(Names.Schema)
                .ForeignColumn("Code")
                .ToTable("ZipCode").InSchema(Names.Schema)
                .PrimaryColumn("Code")
                .OnDeleteOrUpdate(Rule.Cascade);

            Create.ForeignKey("PartOfToArea")
                .FromTable("PartOf").InSchema(Names.Schema)
                .ForeignColumn("AreaID")
                .ToTable("Area").InSchema(Names.Schema)
                .PrimaryColumn("AreaID")
                .OnDeleteOrUpdate(Rule.Cascade);
        }

        public override void Down()
        {
            Delete.ForeignKey("PartOfToArea").OnTable("PartOf").InSchema(Names.Schema);
            Delete.ForeignKey("PartOfToZipCode").OnTable("PartOf").InSchema(Names.Schema);

            Delete.Table("PartOf").InSchema(Names.Schema);
        }
    }
}
