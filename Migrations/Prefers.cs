﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;
using FluentMigrator.Expressions;

namespace Migrations
{
    [Migration(7)]
    public class Prefers : Migration
    {
        public override void Up()
        {
            Create.Table("Prefers").InSchema(Names.Schema)
                .WithColumn("cid").AsInt64().PrimaryKey()
                .WithColumn("StoreID").AsInt64().PrimaryKey()
                .WithColumn("StoreOrder").AsInt64().NotNullable();

            /*  
             * These two foreign keys -should- exist and both cascade.  
             * SQL Server is not allowing both of them to cascade, and since we'll 
             * cascade the Delete in the NHibernate mapping, there won't be too 
             * negative consequences of not setting the cascade action as desired here
            */

            Create.ForeignKey("PrefersToStore")
                .FromTable("Prefers").InSchema(Names.Schema)
                .ForeignColumn("StoreID")
                .ToTable("Store").InSchema(Names.Schema)
                .PrimaryColumn("StoreID")
                .OnDeleteOrUpdate(Rule.None);

            Create.ForeignKey("PrefersToCustomer")
                .FromTable("Prefers").InSchema(Names.Schema)
                .ForeignColumn("cid")
                .ToTable("Customer").InSchema(Names.Schema)
                .PrimaryColumn("cid")
                .OnDeleteOrUpdate(Rule.None);
        }

        public override void Down()
        {
            Delete.ForeignKey("PrefersToCustomer").OnTable("Prefers").InSchema(Names.Schema);
            Delete.ForeignKey("PrefersToStore").OnTable("Prefers").InSchema(Names.Schema);
            Delete.Table("Prefers").InSchema(Names.Schema);
        }
    }
}
