﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Migrations
{
    [Migration(8)]
    public class Video : Migration
    {
        public override void Up()
        {
            Create.Table("Video").InSchema(Names.Schema)
                .WithColumn("VideoID").AsInt64().Identity().PrimaryKey()
                .WithColumn("StoreID").AsInt64().NotNullable()
                .WithColumn("DatePurchased").AsDateTime().NotNullable()
                .WithColumn("NewArrival").AsBoolean().NotNullable();

            Create.ForeignKey("VideoToStore")
                .FromTable("Video").InSchema(Names.Schema)
                .ForeignColumn("StoreID")
                .ToTable("Store").InSchema(Names.Schema)
                .PrimaryColumn("StoreID")
                .OnDeleteOrUpdate(Rule.Cascade);                
        }

        public override void Down()
        {
            Delete.ForeignKey("VideoToStore").OnTable("Video").InSchema(Names.Schema);
            Delete.Table("Video").InSchema(Names.Schema);
        }
    }
}
